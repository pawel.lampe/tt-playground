import json

import zmq


state = {
    'map:version': 0,
    'map:width': 200,
    'map:height': 200,
    'map:tiles:10:15': 1,
}

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind('ipc:///tmp/map.service')

while True:
    message = socket.recv()
    print('received:', message)
    socket.send(json.dumps(state).encode())

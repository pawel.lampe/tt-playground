import logging
import sys
import json

import pygame
import zmq


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

logging.info('reading map...')

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('ipc:///tmp/map.service')

socket.send(b'')
message = socket.recv().decode()
logging.debug('message: %s', message)
state = json.loads(message)

logging.info('rendering map...')

pygame.init()
window = pygame.display.set_mode((state['map:width']*5, state['map:height']*5))
pygame.draw.rect(window, (0xff, 0xff, 0xff), [0, 0, state['map:width']*5, state['map:height']*5])
pygame.display.flip()

import time
time.sleep(5)

def render_tiles(window, tiles):
    for x, y, val in tiles:
        if val == 1:
            color = (0x0,0x0,0x0)
        else:
            color = (0xff,0xff,0xff)
        pygame.draw.rect(window, color, [x*5, y*5, 5, 5])

    pygame.display.flip()

tiles = [(int(k.split(':')[-2]), int(k.split(':')[-1]), v) for k, v in state.items()
         if k.startswith('map:tiles')]

render_tiles(window, tiles)

time.sleep(5)

pygame.quit()
